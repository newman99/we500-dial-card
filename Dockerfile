FROM php:7-fpm

MAINTAINER "Matthew Newman" <newman99@gmail.com>

ENTRYPOINT ["docker-php-entrypoint"]

WORKDIR /var/www/html

# Additional RUN commands here
# RUN yum clean all && \
#    yum -y install <stuff>
RUN apt-get update && apt-get install -y imagemagick

EXPOSE 9000
CMD ["php-fpm"]
