<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <title>Western Electric 500 Dial Dial Card Maker</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="src/css/bootstrap/css/bootstrap.css" rel="stylesheet">
    <style>
      body {
        padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
      }
    </style>
    <link href="src/css/bootstrap/css/bootstrap-responsive.css" rel="stylesheet">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="../assets/js/html5shiv.js"></script>
    <![endif]-->

    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="src/css/bootstrap/css/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
      <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
                    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
                                   <link rel="shortcut icon" href="../assets/ico/favicon.png">

<?php
    $area_code=$_POST["area_code"];
    $local_number=$_POST["local_number"]; 
?>
</head>
<body>
<div class="container">
    <h2>Western Electric 500 Dial Card Maker</h2>
    <div class="row-fluid">
        <div class="span3">
            <form method="post" action="./phone.php">
                <fieldset>
                    <legend></legend>
                    <label for="area_code">Area Code</label><input type="text" class="input-mini" name="area_code" id="area_code" value="<?php echo $area_code?>" required />
                    <label for="local_number">Local Number</label><input type="text" class="input-small" name="local_number" id="local_number" value="<?php echo $local_number?>" required /><br/>
                    <button type="submit" class="btn">Create</button>
                </fieldset>
            </form>
        </div>
        <div class="span5">
<?php
$time=microtime(1)*10000;
$folder="src";

$f1="areacode_".$time.".bmp";
$f2="phone_number_card_ac".$time.".bmp";
$f3="phone_number_".$time.".bmp";

$command="
convert -background white -fill black -font fonts/OpenSans-SemiBold.ttf -pointsize 16 label:\"$area_code\" $folder/$f1;
composite -geometry +90+62 $folder/$f1 img/phone_number_card_blank.png $folder/$f2;
convert -background white -fill black -font fonts/KIN668.TTF -pointsize 16 label:\"$local_number\" $folder/$f1;
composite -density 600 -geometry +46+87 $folder/$f1 $folder/$f2 $folder/$f3;
rm $folder/$f1;
rm $folder/$f2";

$output=shell_exec($command);

if ($area_code) {
    echo "<img src=\"src/$f3\" alt=\"Test Image\"/>";
} else {
    echo "<img src=\"img/phone_number_card_blank.png\" alt=\"Test Image\"/>";
}
?>
        </div>
    </div>
</div>
</body>
</html>
